import QtQuick 2.0
import Lomiri.Components 0.1
import QtQuick.Layouts 1.0
import "../components"
import Machines 1.0

Overlay {
    id: root
    color: app.backgroundColor
    property bool showDeveloperOptions: false

    Column {
        anchors.fill: parent
        anchors.margins: app.margins
        spacing: app.margins

        GameLabel {
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: app.titleSize
            font.bold: true
            text: root.showDeveloperOptions ? "Settings" : i18n.tr("Sound settings")
        }

        Row {
            width: parent.width
            spacing: app.margins
            visible: root.showDeveloperOptions
            Rectangle {
                width: parent.width - soundHeaderLabel.width -  app.margins
                height: units.dp(1)
                anchors.verticalCenter: parent.verticalCenter
                color: app.textColor
            }
            GameLabel {
                id: soundHeaderLabel
                text: i18n.tr("Sound")
            }
        }

        GridLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            columnSpacing: app.margins
            width: parent.width * 0.8

            GameLabel {
                text: i18n.tr("sound enabled")
            }
            CheckBox {
                checked: SettingsBackend.audioEnabled
                onCheckedChanged: SettingsBackend.audioEnabled = checked
            }
            GameLabel {
                text: i18n.tr("soundtrack volume")
            }

            Slider {
                width: parent.width - x
                minimumValue: 0
                maximumValue: 100
                value: SettingsBackend.volume
                onValueChanged: SettingsBackend.volume = value
            }

            GameLabel {
                text: i18n.tr("effects volume")
            }
            Slider {
                width: parent.width - x
                value: SettingsBackend.effectsVolume
                onValueChanged: SettingsBackend.effectsVolume = value
            }
        }

        Row {
            width: parent.width
            spacing: app.margins
            visible: root.showDeveloperOptions
            Rectangle {
                width: parent.width - develHeaderLabel.width -  app.margins
                height: units.dp(1)
                anchors.verticalCenter: parent.verticalCenter
                color: app.textColor
            }
            GameLabel {
                id: develHeaderLabel
                text: i18n.tr("Levelpack development")
            }
        }

        GridLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 4
            visible: root.showDeveloperOptions
            width: parent.width * 0.8
            GameLabel {
                text: i18n.tr("field overlay")
            }
            Row {
                Layout.fillWidth: true
                CheckBox {
                    id: fieldOverlayCheckbox
                    checked: SettingsBackend.fieldOverlayEnabled
                    onCheckedChanged: SettingsBackend.fieldOverlayEnabled = checked
                }
            }
            GameLabel {
                text: i18n.tr("all unlocked")
            }
            Row {
                Layout.fillWidth: true
                CheckBox {
                    checked: SettingsBackend.allUnlocked
                    onCheckedChanged: SettingsBackend.allUnlocked = checked
                }
            }
        }
    }
}
