import QtQuick 2.2
import Lomiri.Components 0.1
import Lomiri.Components.ListItems 0.1
import "../../components"
import Machines 1.0
import QtQuick.Layouts 1.1

ModalOverlay {
    id: root
    title: i18n.tr("Credits")
    showCloseButton: true

    Flickable {
        anchors.fill: parent
        anchors.margins: app.margins
        contentHeight: columnLayout.height
        clip: true
        ColumnLayout {
            id: columnLayout
            width: parent.width
            spacing: app.margins

            GameLabel {
                font.pixelSize: app.titleSize
                font.bold: true
                text: i18n.tr("Game development")
            }

            GameLabel {
                text: "<b>" + i18n.tr("Development") + ":</b> Michael Zanetti"
            }
            GameLabel {
                text: "<b>" + i18n.tr("Design/Artwork") + ":</b> Michał Prędotka"
            }

            ThinDivider {}

            GameLabel {
                font.pixelSize: app.titleSize
                font.bold: true
                text: i18n.tr("Level packs")
            }

            Repeater {
                model: engine.levelPacks

                ColumnLayout {
                    width: parent.width

                    GameLabel {
                        Layout.fillWidth: true
                        text: model.name
                        font.bold: true
                    }
                    GameLabel {
                        Layout.fillWidth: true
                        text: model.copyright
                        wrapMode: Text.WordWrap
                    }
                }
            }
        }
    }
}
