import QtQuick 2.2
import Lomiri.Components 0.1
import Lomiri.Components.ListItems 0.1
import "../../components"
import Machines 1.0
import QtQuick.Layouts 1.1

ModalOverlay {
    id: root
    title: i18n.tr("Let us know what you think!")
    showCloseButton: true

    Column {
        anchors.fill: parent
        anchors.margins: app.buttonSize
        spacing: app.margins

        Row {
            anchors { left: parent.left; right: parent.right; margins: app.margins }
            height: parent.height - y

            spacing: app.buttonSize / 2

            Button {
                height: parent.height
                width: (parent.width - parent.spacing*2 - orLabel.width) / 2
                color: app.confirmationButtonColor
                onClicked: Qt.openUrlExternally("https://open.uappexplorer.com/app/neopar.mvm")

                Label {
                    anchors.fill: parent
                    anchors.margins: app.margins
                    wrapMode: Text.WordWrap
                    text: i18n.tr("Leave us a review in the Open Store")
                    color: app.buttonTextColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            GameLabel {
                id: orLabel
                anchors.verticalCenter: parent.verticalCenter
                text: i18n.tr("or")
            }

            Button {
                height: parent.height
                width: (parent.width - parent.spacing*2 - orLabel.width) / 2
                color: app.confirmationButtonColor
                onClicked: {
                    Qt.openUrlExternally("https://gitlab.com/neopar/mvm")
                }
                Label {
                    anchors.fill: parent
                    anchors.margins: app.margins
                    wrapMode: Text.WordWrap
                    text: i18n.tr("Visit our Gitlab page to report a bug or contact us.")
                    color: app.buttonTextColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }
    }
}
