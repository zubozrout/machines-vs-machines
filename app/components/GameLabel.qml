import QtQuick 2.2
import Lomiri.Components 0.1

Label {
    color: app.textColor
    font.pixelSize: app.fontSize
}
