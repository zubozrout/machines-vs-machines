import QtQuick 2.0
import Lomiri.Components 0.1

BorderImage {
    anchors { right: parent.right; bottom: parent.bottom }
    width: Math.min(parent.width, Math.max(height, countLabel.implicitWidth + height/2))
    height: units.gu(3)
    source: "../graphics/notification.sci"

    property string price
    property int maxWidth: units.gu(5)

    GameLabel {
        id: countLabel
        text: parent.price + i18n.tr("$")
        anchors.centerIn: parent
        width: parent.maxWidth - parent.height
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideRight
        color: "white"
    }
}
